﻿using System;
using Andrii.Salii.RobotChallange;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void isDistanceNotNegativeTest()
        { int finishedPosition;
            finishedPosition = DistanceHelper.FindDistance(new Robot.Common.Position(0, 0), new Robot.Common.Position(0, 0));
            Assert.IsTrue(finishedPosition >= 0);
        }

        [TestMethod]
        public void isDistanceNotNullTest()
        {
            int finishedPosition;
            finishedPosition = DistanceHelper.FindDistance(new Robot.Common.Position(0, 0), new Robot.Common.Position(0, 0));
            Assert.IsNotNull(finishedPosition > 0);
        }

        [TestMethod]
        public void isPositionEquals()
        {
            Assert.AreEqual(new Position(0, 0), new Position(0, 0));
        }
        [TestMethod]
        public void isPositionNotEquals()
        {
            Assert.AreNotEqual(new Position(0, 0), new Position(0, 1));
        }

        [TestMethod]
        public void isRobotNotEqulas()
        {
            Assert.AreNotEqual(new Robot.Common.Robot(), new Robot.Common.Robot());
        }

        [TestMethod]
        public void isStationsPositionEqulas()
        {
            EnergyStation station1 = new EnergyStation();
            EnergyStation station2 = new EnergyStation();
            station1.Position = new Position(0, 1);
            station2.Position = new Position(0, 1);
            Assert.AreEqual(station1.Position, station2.Position);
        }

        [TestMethod]
        public void isStationsNotEqulas()
        {
            EnergyStation station = new EnergyStation();
            station.Position = new Position(1, 2);
            Assert.AreNotEqual(new EnergyStation(),station);
        }

        [TestMethod]
        public void isNextPositionNotNull()
        {
            Position nextPosition;
            AndriiSaliiAlgorithm algorithm = new AndriiSaliiAlgorithm();
            nextPosition = algorithm.CalculateNextPoint(new Robot.Common.Position(0, 0), new Robot.Common.Position(0, 0),100);
            Assert.IsNotNull(nextPosition);
        }

        [TestMethod]
        public void isOnPositionTest()
        {
            AndriiSaliiAlgorithm algorithm = new AndriiSaliiAlgorithm();

            Assert.IsTrue(algorithm.isOnPosition(new Robot.Common.Position(0, 0), new Robot.Common.Position(0, 0)));
        }

        [TestMethod]
        public void isNotOnPositionTest()
        {
            AndriiSaliiAlgorithm algorithm = new AndriiSaliiAlgorithm();

            Assert.IsFalse(algorithm.isOnPosition(new Robot.Common.Position(0, 0), new Robot.Common.Position(1, 1)));
        }

        [TestMethod]
        public void isOnPositionNotNullTest()
        {
            AndriiSaliiAlgorithm algorithm = new AndriiSaliiAlgorithm();

            Assert.IsNotNull(algorithm.isOnPosition(new Robot.Common.Position(0, 0), new Robot.Common.Position(0, 0)));
        }

    }
}
