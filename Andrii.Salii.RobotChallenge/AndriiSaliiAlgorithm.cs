﻿using Robot.Common;
using System;
using System.Collections.Generic;
namespace Andrii.Salii.RobotChallange
{
    public class AndriiSaliiAlgorithm : IRobotAlgorithm
    {
        private List<EnergyStation> mStations = new List<EnergyStation>();
        public int RoundCount
        {
            get;
            set;
        }
        public string Author
        {
            get
            {
                return "Andrii Salii";
            }
        }
        public string Description
        {
            get
            {
                return "Andrii Salii";
            }
        }
        public AndriiSaliiAlgorithm()
        {
            Logger.OnLogRound += new LogRoundEventHandler(this.Logger_OnLogRound);
        }
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            int roundCount = this.RoundCount;
            this.RoundCount = roundCount + 1;
        }
       public bool IAmOnStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            bool outcome;
            foreach (EnergyStation current in map.Stations)
            {
                bool isOnPosition = this.isOnPosition(movingRobot.Position , current.Position);
                if (isOnPosition)
                {
                    outcome = true;
                    return outcome;
                }
            }
            outcome = false;
            return outcome;
        }
        public bool isOnPosition(Position start,Position end)
        {
            int startX = start.X;
            int startY = start.Y;
            int endX = end.X;
            int endY = end.Y;

            return (Math.Abs(startX - endX) == 0 && Math.Abs(startY - endY)==0);
        }
        public Position FindStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            int int_ = 2147483647;
            Position outcome = null;
            foreach (EnergyStation current in map.Stations)
            {
                bool isFree = this.IsStationFree(movingRobot, current, robots);
                if (isFree)
                {
                    int int_2 = DistanceHelper.FindDistance(current.Position, movingRobot.Position);
                    bool bool_2 = int_2 < int_;
                    if (bool_2)
                    {
                        int_ = int_2;
                        outcome = current.Position;
                    }
                }
            }
            return outcome;
        }
        public bool IsStationFree(Robot.Common.Robot movingRobot, EnergyStation station, IList<Robot.Common.Robot> robots)
        {
            int int_ = station.Position.X - 1;
            bool outcome;
            while (int_ <= station.Position.X + 1)
            {
                for (int int_2 = station.Position.Y - 1; int_2 <= station.Position.Y + 1; int_2++)
                {
                    foreach (Robot.Common.Robot current in robots)
                    {
                        bool bool_ = current.Position == new Position(int_, int_2);
                        if (bool_)
                        {
                            outcome = false;
                            return outcome;
                        }
                    }
                }
                int_++;
                continue;
                return outcome;
            }
            outcome = true;
            return outcome;
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot myRobot, IList<Robot.Common.Robot> robots)
        {
            bool outcome;
            foreach (Robot.Common.Robot current in robots)
            {
                bool bool_ = current != myRobot;
                if (bool_)
                {
                    bool bool_2 = current.Position == cell;
                    if (bool_2)
                    {
                        outcome = false;
                        return outcome;
                    }
                }
            }
            outcome = true;
            return outcome;
        }
        public Position CalculateNextPoint(Position Position1, Position desiredPosition, int MaxDistanceEnergy)
        {
            Position position = new Position(desiredPosition.X, desiredPosition.Y);
            Position outcome;
            for (int i = 50; i > 0; i--)
            {
                int int_ = DistanceHelper.FindDistance(Position1, position);
                bool bool_ = int_ < MaxDistanceEnergy;
                if (bool_)
                {
                    outcome = position;
                    return outcome;
                }
                position.X = Position1.X + (int)((double)(position.X - Position1.X) / 2.0);
                position.Y = Position1.Y + (int)((double)(position.Y - Position1.Y) / 2.0);
            }
            bool bool_2 = position.X < 0 || position.X > 50 || position.Y < 0 || position.Y > 50;
            if (bool_2)
            {
                outcome = Position1;
                return outcome;
            }
            outcome = position;
            return outcome;
        }
        public Position Destination(IList<Robot.Common.Robot> robots, Robot.Common.Robot myRobot, Position desiredPosition)
        {
            int int_ = (myRobot.Energy > 300) ? 300 : myRobot.Energy;
            Position position = new Position(desiredPosition.X, desiredPosition.Y);
            Position position2 = new Position(myRobot.Position.X, myRobot.Position.Y);
            bool bool_ = Math.Abs(position2.X - position.X) + Math.Abs(position2.Y - position.Y) + 50 > int_;
            Position outcome;
            if (bool_)
            {
                outcome = null;
            }
            else
            {
                int int_2 = 0;
                int int_3 = 50;
                while (myRobot.Energy < int_ * int_2 || int_3 == 50)
                {
                    bool bool_2 = int_3 != 50;
                    if (bool_2)
                    {
                        int_ = int_ * 5 / 6;
                    }
                    int_2 = 0;
                    position2 = myRobot.Position;
                    int int_4 = 50;
                    while (position2 != desiredPosition && int_4 > 0)
                    {
                        int_4--;
                        int_2++;
                        Position position3 = this.CalculateNextPoint(position2, position, int_);
                        position2 = position3;
                    }
                    int_3--;
                    bool bool_3 = int_3 == 0;
                    if (bool_3)
                    {
                        break;
                    }
                }
                bool bool_4 = int_3 <= 0;
                if (bool_4)
                {
                    outcome = null;
                }
                else
                {
                    Position position3 = this.CalculateNextPoint(myRobot.Position, position, int_);
                    outcome = position3;
                }
            }
            return outcome;
        }
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot robot = robots[robotToMoveIndex];
            bool isOnStation = this.IAmOnStation(robot, map, robots);
            RobotCommand outcome;
            if (isOnStation)
            {
                bool isEnoughtEnergyForSpread = robot.Energy > 300 && robots.Count < map.Stations.Count && this.RoundCount < 40;
                if (isEnoughtEnergyForSpread)
                {
                    CreateNewRobotCommand varID = new CreateNewRobotCommand();
                    varID.NewRobotEnergy = 100;
                    outcome = varID;
                }
                else
                {
                    outcome = new CollectEnergyCommand();
                }
            }
            else
            {
                Position nextPosition = this.FindStation(robot, map, robots);
                bool isNextPositionAvailable = nextPosition == null;
                if (isNextPositionAvailable)
                {
                    Position varMV = new Position();
                    varMV.X = robot.Position.X + 1;
                    varMV.Y = robot.Position.Y + 1;
                    Position position2 = varMV;
                    nextPosition = position2;
                }
                Position nextPositionAnother = null;
                bool isNextPosition = nextPosition != null;
                if (isNextPosition)
                {
                    nextPosition = this.Destination(robots, robot, nextPosition);
                }
                bool bool_5 = nextPositionAnother == null;
                if (bool_5)
                {
                    outcome = new CollectEnergyCommand();
                }
                else
                {
                    MoveCommand varEE = new MoveCommand();
                    varEE.NewPosition = nextPositionAnother;
                    outcome = varEE;
                }
            }
            return outcome;
        }
    }
}
