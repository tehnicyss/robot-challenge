﻿using Robot.Common;
using System;
namespace Andrii.Salii.RobotChallange
{
    public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow((double)(a.X - b.X), 2.0) + Math.Pow((double)(a.Y - b.Y), 2.0));
        }
    }
}
